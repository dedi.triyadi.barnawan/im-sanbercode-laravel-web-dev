<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        // Validasi
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        // Insert Data
        DB::table('cast')->insert([
            'nama' => $request["nama"],
            'umur' => $request["umur"],
            'bio' => $request["bio"],
        ]);

        return redirect('/cast');
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('cast.index', ['cast' => $cast]);
    }

    public function show($id)
    {
        $castById = DB::table('cast')->find($id);
        return view('cast.detail', ['castById' => $castById]);
    }
    public function edit($id)
    {
        $castById = DB::table('cast')->find($id);
        return view('cast.edit', ['castById' => $castById]);
    }

    public function update($id, Request $request)
    {
        // Validasi
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        // Insert Data
        DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request["nama"],
                'umur' => $request["umur"],
                'bio' => $request["bio"]
            ]);

        return redirect("/cast");
    }

    public function destroy($id)
    {
        $query = DB::table("cast")->where("id", $id)->delete();
        return redirect('/cast');
    }


}
