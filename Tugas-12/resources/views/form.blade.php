<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="firstname" id=""><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="lastname" id=""><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="male">Male<br>
        <input type="radio" name="gender" value="famale">Famale<br>
        <input type="radio" name="gender" value="other">Other<br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="singapura">Singapura</option>
            <option value="malaysia">Malaysia</option>
            <option value="amerika">Amerika</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="language" id="">Bahasa Indonesia<br>
        <input type="checkbox" name="language" id="">English<br>
        <input type="checkbox" name="language" id="">Other<br><br>
        <label>Bio:</label><br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>