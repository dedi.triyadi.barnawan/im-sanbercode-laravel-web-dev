1. SOAL No.1
create database myshop;

2. SOAL NO.2
create table users(id int AUTO_INCREMENT PRIMARY KEY, name varchar(255), email varchar(255), password varchar(255));
create table categories(id int AUTO_INCREMENT PRIMARY KEY, name varchar(255));
create table items(id int AUTO_INCREMENT PRIMARY KEY, name varchar(255), description varchar(255), price int, stock int, categories_id int, FOREIGN KEY (categories_id) REFERENCES categories(id));

3. SOAL No.3
insert INTO users (name, email, password) VALUES ("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");
insert INTO categories (name) VALUES ("gadget"), ("cloth"), ("men"), ("women"), ("branded");
insert INTO items (name, description, price, stock, categories_id) VALUES ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);


4. SOAL No.4
4-a SELECT id,name,email FROM users;
4-b SELECT * FROM `items` WHERE price > 1000000; 
    SELECT * FROM `items` WHERE name LIKE 'uniklo%';
4-c select items.*, categories.name as kategori from items INNER JOIN categories ON items.categories_id = categories.id;

5 SOAL no.5
UPDATE items SET price=2500000 where id =1;